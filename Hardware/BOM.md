# 3D Printed Parts
## Legs
https://www.thingiverse.com/thing:2901132/files

- m-femur.stl
- m-tibia.stl
- m-coxa.stl

## Body
https://www.thingiverse.com/thing:3525004

# Electronica
## Servos
12 x SG90 - https://www.aliexpress.com/item/32561052304.html?spm=a2g0s.9042311.0.0.27424c4drjT8eO

## Arduino 
1 x Arduino Nano

## Power
1 x Buck Converter 7-24v to 5v

## Battery Options
1 x 9v Battery
2 x 18650
