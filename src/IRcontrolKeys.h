#ifndef __IR_CODES__
#define __IR_CODES__

/* IR codes -----------------------------------------------------------------*/
#define KEY_0         0
#define KEY_1         1
#define KEY_2         2 
#define KEY_3         3
#define KEY_4         4
#define KEY_5         5
#define KEY_6         6
#define KEY_7         7
#define KEY_8         8
#define KEY_9         9
#define KEY_FORWARD   10
#define KEY_BACKWARD  11
#define KEY_RIGHT     12
#define KEY_LEFT      13
#define KEY_STAND     14
#define KEY_AUTO      15
#define KEY_CONTROL   16

constexpr unsigned long ir_control_keys[]={
  0x6E56F924, // key 0
  0x92DF9279, // key 1
  0x87CDD0EF, // key 2
  0x37788763, // key 3
  0xA519853B, // key 4
  0x5CDD8FBD, // key 5
  0x42DD49, // key 6
  0x31BB009F, // key 7
  0x153F90A7, // key 8
  0x6872B60C, // key 9
  0x2C2E80FF, // forward, arrow up key
  0x5A1A483D, // backward, arrow down key
  0xDC18602C, // right, arrow right key
  0x9578646A, // left, arrow left key 
  0xCB3CC07F, // stand, middle key
  0x19EB3488, // auto, menu key, 1st column most rightnest key
  0x4666CC0D  // control, power key, 1st column most left key
};

#endif
